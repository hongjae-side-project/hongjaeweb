FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE=build/libs/*SNAPSHOT.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-javaagent:\"$PINPOINT_PATH/pinpoint-bootstrap-2.2.2.jar\"","-Dpinpoint.config=$PINPOINT_PATH/pinpoint-root.config","-Dpinpoint.agentId=lhj01","-Dpinpoint.applicationName=SpringBootApp","-jar","/app.jar"]