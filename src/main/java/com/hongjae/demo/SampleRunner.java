package com.hongjae.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class SampleRunner implements ApplicationRunner {

    @Value("${hongjae.name}")
    private String name;

    @Autowired
    private String hello;

    @Autowired
    private HongJaeProperties hongJaeProperties;

    private Logger logger = LoggerFactory.getLogger(SampleRunner.class);

    @Override
    public void run(ApplicationArguments args) throws Exception {

        System.out.println("================================");
        System.out.println("SampleRunner:"+name);
        System.out.println("================================");
        System.out.println("hello:"+hello);
        System.out.println("================================");
        System.out.println("HongJaeProperties:"+hongJaeProperties.getName());
        System.out.println("================================");

        // logger info출력
        logger.info("--------------------------------------");
        logger.info(hello);
        logger.info(hongJaeProperties.getName());
        logger.info("--------------------------------------");

        // logger debug출력
        logger.debug("--------------------------------------");
        logger.debug(hello);
        logger.debug(hongJaeProperties.getName());
        logger.debug("--------------------------------------");

    }
}
