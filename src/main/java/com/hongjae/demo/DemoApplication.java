package com.hongjae.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication // 항상 프로젝트 최상단에 위치해야함(이 위치부터 설정을 읽어나가기 때문)
public class DemoApplication {

	public static void main(String[] args) {
		//SpringApplication.run(DemoApplication.class, args);
		SpringApplication app = new SpringApplication(DemoApplication.class);
		app.addListeners(new StartingListener()); // AppliccationContext를 만들기 전에 사용하는 리스너는 @bean으로 등록할 수 없어 직접 추가
		app.run(args);
	}
}
