package com.hongjae.demo;

import org.springframework.boot.context.event.ApplicationStartingEvent;
import org.springframework.context.ApplicationListener;


public class StartingListener implements ApplicationListener<ApplicationStartingEvent> {
    @Override
    public void onApplicationEvent(ApplicationStartingEvent event) {
        System.out.println("=======================================");
        System.out.println("1. ApplicationStartingEvent Listener");
        System.out.println("=======================================");
    }
}
