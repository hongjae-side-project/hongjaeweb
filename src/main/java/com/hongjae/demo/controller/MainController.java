package com.hongjae.demo.controller;

import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    Logger logger = LoggerFactory.getLogger(MainController.class);

    @ApiOperation("Hello World Test") // swagger에서 사용하는 어노테이션으로 API의 간단한 설명
    @GetMapping("/hello")
    public String[] hello() {
        logger.info("logger");
        System.out.println("sout");
        return new String[]{"Hello","World"};
    }

    @GetMapping("/my")
    public String my() {
        return "my";
    }
}
