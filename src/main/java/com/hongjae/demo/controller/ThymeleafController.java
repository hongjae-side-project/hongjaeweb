package com.hongjae.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ThymeleafController {

    @GetMapping("/thymeleaf1")
    public String thymeleaf1(Model model) {
        model.addAttribute("name","hongjae");
        return "thymeleaf1"; // html과 동일한 이름으로 return해줘야 함(ex:thymeleaf1.html)
    }
}
