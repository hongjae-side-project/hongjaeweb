package com.hongjae.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable() // POST METHOD 403 error 방지
                .authorizeRequests()
                 //   .antMatchers("/","/hello").permitAll() // 로그인 없이 접속허용
                 //   .antMatchers("/","/my").permitAll()
                .antMatchers("/**").permitAll() // 모든 경로 허용
                    .anyRequest().authenticated()
                    .and()
                .formLogin()
                     .and()
                .httpBasic();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        //return NoOpPasswordEncoder.getInstance(); // password 인코더 안쓴다는 것인데 실제로는 쓰지말기!!!
        return PasswordEncoderFactories.createDelegatingPasswordEncoder(); // password 인코딩해서 저장
    }
}
