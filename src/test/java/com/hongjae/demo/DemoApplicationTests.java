package com.hongjae.demo;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:/test.properties") // 테스트용 properties(별도 선언하지 않으면 application.properties바라본다.)
@SpringBootTest
class DemoApplicationTests {

	@Test
	void contextLoads() {
		System.out.println("contextLoads...");


	}

}
