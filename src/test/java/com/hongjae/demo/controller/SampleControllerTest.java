package com.hongjae.demo.controller;


import com.hongjae.demo.service.SampleService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.as;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

//@SpringBootTest(webEnvironment =  SpringBootTest.WebEnvironment.MOCK) // 이게 기본이다
@SpringBootTest(webEnvironment =  SpringBootTest.WebEnvironment.RANDOM_PORT) // 실제 서블릿이 뜬다
//@SpringBootTest
@AutoConfigureMockMvc // @AutoConfigureMockMvc는 @SpringBootTest 애노테이션과 함께 작성하여, @WebMvcTest와 비슷하게 사용할 수 있다.
/*
@WebMvcTest와 차이점은 @AutoConfigureMockMvc는 컨트롤러뿐만 아니라
@Service, @Repository가 분은 객체들도 모두 메모리에 올린다는 점이다.
즉, 컨트롤러만 테스트할 때는 @WebMvcTest를 이외에 컴포넌트들도 테스트하려면 @AutoConfigureMockMvc를 사용하자.
또한 @WebMvcTest, @SpringBootTest는 모두 MockMvc를 모킹하기 때문에 충돌이 발생하여 함께 사용할 수 없다.
 */
class SampleControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private SampleController sampleController;

    @MockBean
    SampleService mockSampleService;

    @Autowired
    TestRestTemplate testRestTemplate;

    // org.junit : Junit4
    // org.junit.jupiter.api : Junit5
    @Test
    public void sample() throws Exception {
//        assertThat(sampleController.sample()).isEqualTo("sample HongJae");

        // Mock service 호출
        when(mockSampleService.getName()).thenReturn("hongjae");

        String result = testRestTemplate.getForObject("/sample", String.class);
        assertThat(result).isEqualTo("sample hongjae");
    }
}