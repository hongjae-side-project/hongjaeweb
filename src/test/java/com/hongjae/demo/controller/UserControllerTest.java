package com.hongjae.demo.controller;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class) // junit4 이하에서는 runwith 어노테이션 이용
@WebMvcTest(UserController.class) // @WebMvcTest는 @Controller, @RestController가 설정된 클래스들을 찾아 메모리에 생성
class UserControllerTest   {
    
    @Autowired
    MockMvc mockMvc; // WebMvcTest선언하면 자동으로 빈에 주입하기때문에 꺼내서 바로 사용가능

    @DisplayName("UserTest1") // 테스트 이름 지정
    @Test
    public void user1() throws Exception {
        mockMvc.perform(get("/user"))
                .andExpect(status().isOk())
                .andExpect(content().string("user"));
    }

    @DisplayName("UserJsonTest")
    @Test
    public void createUser_JSON() throws Exception {
        String userJson = "{\"id\":\"1\",\"username\":\"hongjae\",\"password\":\"123\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/users/create")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON) //응답
                .content(userJson)) // 응답본문
        .andExpect(status().isOk())
        .andExpect((ResultMatcher) jsonPath("$.username", Matchers.is(equalTo("hongjae"))))
        .andExpect((ResultMatcher) jsonPath("$.password",Matchers.is(equalTo("123"))));
    }

    @DisplayName("ViewResolveTest")
    @Test
    public void createUser_XML() throws Exception { // 요청은 JSON / 응답은 XML
        String userJson = "{\"id\":\"1\",\"username\":\"hongjae\",\"password\":\"123\"}";

        mockMvc.perform(MockMvcRequestBuilders.post("/users/create")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_XML) //응답
                .content(userJson)) // 응답본문
                .andExpect(status().isOk())
                .andExpect(xpath("/User/username").string("hongjae"))
                .andExpect(xpath("User/password").string("123"));
    }



}